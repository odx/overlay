## My Gentoo Linux Overlay
Consists of personal ebuilds and patches for various things I couldn't find elsewhere, or just wanted my own.  
Most of things in this repo I use at least on a weekly basis, therefore it should stay fairly maintained.  
I can't guarantee you breakage free experience, or even sane code/ebuilds in general. Use at your own risk!
### Installation
Using eselect-repository:
```
eselect repository add odx git https://gitlab.com/odx/overlay.git
emerge --sync odx
```

Manually:

Create the overlay directory (you can choose your own location, edit it in odx.conf as well)
```
mkdir -p /usr/local/portage/overlay/odx
chown -R portage:portage /usr/local/portage
```
Add this to the `/etc/portage/repos.conf/odx.conf`
```
[odx]
location   = /usr/local/portage/overlay/odx
sync-type  = git
sync-uri   = https://gitlab.com/odx/overlay.git
auto-sync  = no
```
Note: I use `no` for `auto-sync` to save time, I know when I update my own repository and when I have to sync.  
Until there are only a few of programs in here I suggest `no` as well, sync manually when you need to install something.
```
emerge --sync odx
```
### Usage
You would need to add `~amd64` keyword for all packages in this repository.  
Make sure you emerge with all needed use flags, new use flags could have been added.  
You can check them with equery, eix, or just browsing the ebuild itself.
```
equery u rxvt-unicode
eix rxvt-unicode
```
