EAPI=7
inherit linux-info linux-mod

DESCRIPTION="Silk Guardian is an anti-forensic LKM kill-switch that monitor USB ports and turns off your computer"
HOMEPAGE="https://github.com/NateBrune/silk-guardian"
SRC_URI="https://github.com/NateBrune/silk-guardian/archive/master.zip"
S="${WORKDIR}/silk-guardian-master"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="sys-kernel/linux-headers"
DEPEND="${RDEPEND}"

MODULE_NAMES="silk(extra:)"

PATCHES=(
        "${FILESDIR}"/config.patch
)
src_configure() {
	set_arch_to_kernel
}
src_compile() {
	emake -j1
}
src_install() {
	linux-mod_src_install
}
