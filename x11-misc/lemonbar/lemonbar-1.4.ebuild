# Copyright 2018-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A featherweight, lemon-scented, bar based on xcb"
HOMEPAGE="https://gitlab.com/odx/lemonbar-xft/-/tree/master"
SRC_URI="https://gitlab.com/odx/lemonbar-xft/-/archive/${PV}/lemonbar-xft-${PV}.tar.gz"
KEYWORDS="~amd64 ~x86"
S="${WORKDIR}/lemonbar-xft-${PV}/"

LICENSE="MIT"
SLOT="0"
IUSE="+xft"

DEPEND="x11-libs/libxcb
	xft? ( x11-libs/libXft )
"
RDEPEND="${DEPEND}
	dev-lang/perl
"
BDEPEND=""
