## Personal patches for slock (suckless) screen locker.
These patches are dependent on each other.  
They cannot be applied separately without additional modifications.
### Important
This slock version has some features which require it to be run as root, these features are:  
Lock TTY switching when slock is running, unlock it afterwards.  
Poweroff the system immediately 1) After 5 failed attempts 2) If any modifier (except Shift) is pressed.  

To use slock with sudo but without typing password you need to edit `/etc/sudoers`, and add to it:  
(make sure you add this line at the very end of that file)  
```
*your_username* ALL=(ALL) NOPASSWD: /sbin/poweroff, /sbin/shutdown, /sbin/reboot, /usr/bin/slock, \
				    /usr/bin/cat /proc/sys/kernel/sysrq
```
### Usage
To start slock after period of inactivity you can use `xss-lock` or `xautolock`, put something like this into your autostart file:  
```
# Lock the screen and send the monitor to standby after 30 min. of inactivity
xset dpms 1800 0 0 &&
# Run the locker through xss-lock
xss-lock -q -- sudo slock &
```
If you want to lock your system manually, don't invoke slock directly (if it's running from your autostart already),  
instead use `xset s activate`, to lock your screen, or `xset dpms force standby` to lock and send the monitor to standby.  

I also use `vlock`, which can lock the active TTY session, I launch my X like this: `xinit -- vt1 &> /dev/null & vlock`  
(Note: vt1 is for virtual terminal, if you run X as root you won't need that). This will lock my TTY even if X is killed.
### Additional measures
It is extremely important to implement some sort of protection against rogue USB devices.  
Such device can essentially overcome everything we've done so far, there are many possible implementations:

[USBGuard](https://usbguard.github.io/)

[USBKill](https://github.com/hephaest0s/usbkill)

[Silk Guardian](https://wiki.gentoo.org/wiki/Silk_Guardian)

[Whitelisting through udev/eudev](https://wiki.gentoo.org/wiki/Allow_only_known_usb_devices)
