## Patched urxvt with true color and powerline support
For true color support emerge with `24-bit-color` and `256-color` use flags.  
For powerline and glyphs support emerge with `wide-glyphs` and `unicode3` use flags.  
Other use flags were left unchanged, add them according to your needs.
### Possible issues
There may be issues with tmux or ssh connections (unrecognized terminal type), some ways you can fix that:  
1) Not recommended, but the easiest way is to add `export TERM='screen-256color'` to your .bashrc on the server.  
2) Install this urxvt version with same use flags on your server, it should work automatically. (skip to compatibility)  
3) (Recommended) Copy your proper urxvt terminfo from your host to the server:
```
scp /usr/share/terminfo/r/rxvt-unicode-256color root@server:/usr/share/terminfo/r

```
To be able to copy to `/usr` or login as root, your server's sshd should be configured to permit root logins.  
You can do it temporarily in `sshd_config`. If you absolutely can't do that, you can copy it to a regular user:
```
scp /usr/share/terminfo/r/rxvt-unicode-256color user@server:.terminfo/r
```
The `.terminfo/r` in user home should be created in advance.  
The downside of copying to regular user is that if you login as root on the server later,  
your terminfo won't be transferred from your regular user, and you will get same problems again.
### Compatibility
Now tmux and ssh should work and show correct colors/fonts.  
However for better compatibility you can assign different TERM value for tmux exclusively:  
Create `~/.tmux.conf` and add `set -g default-terminal "tmux-256color"` to it.

For powerline/glyphs you need special nerd/powerline fonts and a lot of tweaking.
